using System;
using UnityEngine;
using Zenject;

namespace Core.PopUp
{
    public interface IPopUpView
    {
        event Action<IPopUpView> CloseClicked;

        void Init(DiContainer container);
        GameObject Body { get; }
        void ShowWithAnim(Action callback = null);
        void HideWithAnim(Action callback = null);
        void Destroy();
    }
}