namespace Core.PopUp
{
    public interface IPopupSystem
    {
        public PopUpViewBase<TModel> OpenPopUp<TModel>(TModel model) where TModel : PopUpModelBase;
    }
}