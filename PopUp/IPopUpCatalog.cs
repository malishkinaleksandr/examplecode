using UnityEngine;

namespace Core.PopUp
{
    public interface IPopUpCatalog
    {
        Canvas HeaderCanvas { get; }
        GameObject GetPrefab(PopUpType popUpType);
    }
}