using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.PopUp.SimpleRealization
{
    [CreateAssetMenu(fileName = "PopUpPrefabsCatalog", menuName = "Configs/PopUpPrefabsCatalog")]
    public class PopUpPrefabsCatalog : ScriptableObject, IPopUpCatalog
    {
        [SerializeField] private Canvas _popUpHeader;
        [SerializeField] private List<CatalogInfo> _prefabs;

        public Canvas HeaderCanvas => _popUpHeader;

        public GameObject GetPrefab(PopUpType popUpType)
        {
            var result = _prefabs.Find(item => item.PopUpType == popUpType);
            if (result == null)
            {
                 Debug.LogError("Cannot find prefab for " + popUpType);
                return null;
            }

            return result.PopUpPref;
        }
    }
    
    [Serializable]
    public class CatalogInfo
    {
        public PopUpType PopUpType;
        public GameObject PopUpPref;
    }
}