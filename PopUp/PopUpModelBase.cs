using Core.ObserverView;

namespace Core.PopUp
{
    public abstract class PopUpModelBase : ObservableBase
    {
        public abstract PopUpType PopUpType { get; }
    }
}