using UnityEngine;

namespace Core.PopUp
{
    public interface IPopUpFactory
    {
        Canvas GetCanvas();
        GameObject Get<TModel>(TModel model)
            where TModel : PopUpModelBase;

        void Release(IPopUpView popUp);
    }
}