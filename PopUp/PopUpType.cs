namespace Core.PopUp
{
    public enum PopUpType
    {
        Unknown = -1,
        OnPopupPause,
        OnPopupDead,
        OnPopupLevelUp,
        OnJumpBoost,
        OnSpeedBoost,
        OnTestProperty,
        OnPropertyPopup,
        OnDamagePopup,
        OnTakeArmPopup,
        OnSettingPopup,
    }
}