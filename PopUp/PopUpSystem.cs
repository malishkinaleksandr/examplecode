using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Core.PopUp
{
    public class PopUpSystem : IPopupSystem
    {
        public event Action<string> OnError;
        
        [Inject] private IPopUpFactory _factory;
        
        private DiContainer _diContainer;
        private RectTransform _parentForPopUps;

        private Dictionary<PopUpModelBase, IPopUpView> _currentPopups = new Dictionary<PopUpModelBase, IPopUpView>();

        private readonly List<IPopUpView> _currentOpenPopUps = new List<IPopUpView>();

        public PopUpSystem(DiContainer container, IPopUpFactory factory)
        {
            _diContainer = container;
            _factory = factory;

            _parentForPopUps = _factory.GetCanvas().GetComponent<RectTransform>();
        }

        public virtual PopUpViewBase<TModel> OpenPopUp<TModel>(TModel model)
            where TModel : PopUpModelBase
        {
            var instance = _factory.Get(model);
            var result = instance.GetComponent<PopUpViewBase<TModel>>();
            result.OnInjectComponents(_diContainer);
            var popup = result as IPopUpView;

            if (IsExist(model))
            {
                return GetExistPopup(model) as PopUpViewBase<TModel>;
            }

            if (popup == null)
            {
                OnError?.Invoke($"Cannot get for {model.PopUpType}");
                return null;
            }
            
            popup.Init(_diContainer);
            result.ApplyModel(model);
            popup.CloseClicked += OnPopUpCloseClicked;
            popup.Body.transform.SetParent(_parentForPopUps, false);
            _currentPopups.Add(model,popup);
            popup.ShowWithAnim();

            return result;
        }

        private bool IsExist<TModel>(TModel model) where TModel : PopUpModelBase
        {
            return _currentPopups.TryGetValue(model, out var view);
        }
        
        private IPopUpView GetExistPopup<TModel>(TModel model) 
            where TModel : PopUpModelBase
        {
            if (_currentPopups.TryGetValue(model, out var view))
            {
                return view;
            }

            return null;
        }

        private void OnPopUpCloseClicked(IPopUpView view)
        {
            view.CloseClicked -= OnPopUpCloseClicked;
            _currentOpenPopUps.Remove(view);
            view.HideWithAnim(() =>
            {
                _factory.Release(view);
                view.Body.transform.SetParent(null);
            });
        }
    }
}