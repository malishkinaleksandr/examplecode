using System;
using System.Collections.Generic;
using Core.ObserverView;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Core.PopUp
{   
    public abstract class PopUpViewBase<TModel> : ObserverViewBase<TModel>, IPopUpView
    where TModel : PopUpModelBase
    {
        public event Action<IPopUpView> CloseClicked;
        
        GameObject IPopUpView.Body => gameObject;
        
        [SerializeField] private List<Button> _btnsClose = new List<Button>();

        void IPopUpView.Init(DiContainer container)
        {
            OnInit();
            container.Inject(this);
        }
        
        void IPopUpView.ShowWithAnim(Action callback)
        {
            Show();
            PlayShowAnim(() =>
            {
                SetSubscription(true);
                callback?.Invoke();
            });
        }

        void IPopUpView.HideWithAnim(Action callback)
        {
            SetSubscription(false);
            PlayHideAnim(() =>
            {
                Hide();
                callback?.Invoke();
            });
        }

        void IPopUpView.Destroy()
        {
            Model.RemoveObserver(this);
            Destroy(gameObject);
        }
        
        protected virtual void SetSubscription(bool isNeedSubscribe)
        {
            if (isNeedSubscribe)
            {
                _btnsClose.ForEach(item => item.onClick.AddListener(OnCloseClicked));
            }
            else
            {
                _btnsClose.ForEach(item => item.onClick.RemoveAllListeners());
            }
        }

        protected virtual void PlayShowAnim(Action callback = null)
        {
            callback?.Invoke();
        }

        protected virtual void PlayHideAnim(Action callback = null)
        {
            callback?.Invoke();
        }

        protected virtual void OnInit()
        {
            
        }

        protected virtual void OnCloseClicked()
        {
            ClosePopUp();
        }

        protected void ClosePopUp()
        {
            CloseClicked?.Invoke(this);
        }
    }
}