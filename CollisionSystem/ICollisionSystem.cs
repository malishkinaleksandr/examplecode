using System;
using GameScene.GameElements.CollisionSystem.CollisionRules;
using GameScene.GameElements.CollisionSystem.Configs;
using UnityEngine;
using Zenject;

namespace GameScene.GameElements.CollisionSystem
{
    public interface ICollisionSystem<T> : IInitializable, IDisposable where T : BaseRule
    {
        void ApproveCollision(ECollisionType collisionType, GameObject otherGameObject);
    }
}