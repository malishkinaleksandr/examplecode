namespace GameScene.GameElements.CollisionSystem.Configs
{
    public interface ICollisionConfig
    {
        ECollisionType CollisionType { get; }
    }
}