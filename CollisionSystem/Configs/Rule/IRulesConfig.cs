namespace GameScene.GameElements.CollisionSystem.Configs.Rule
{
    public interface IRulesConfig 
    {
        ICollisionConfig[] Configs { get; }
    }
}