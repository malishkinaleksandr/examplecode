using Core.Signals;
using GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules.Signals;
using MoreMountains.NiceVibrations;

namespace GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules
{
    public class JumpRule : BaseBoostRule
    {
        public JumpRule(ISignalSender signalSender) : base(signalSender, BoostType.JUMP)
        {
        }
        
        public override void SetRule()
        {
            base.SetRule();

            MMVibrationManager.Haptic(HapticTypes.LightImpact);
            _signalSender.SendEmpty<JumpBoostSignal>();
        }
    }
}