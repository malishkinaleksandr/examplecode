namespace GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules
{
    public enum BoostType
    {
        HEALTH,
        JUMP,
        SPEED
    }
}