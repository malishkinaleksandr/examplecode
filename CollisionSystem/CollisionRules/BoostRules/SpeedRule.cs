using Core.Signals;
using GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules.Signals;

namespace GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules
{
    public class SpeedRule : BaseBoostRule
    {
        public SpeedRule(ISignalSender signalSender) : base(signalSender, BoostType.SPEED)
        {
        }

        public override void SetRule()
        {
            base.SetRule();

            _signalSender.SendEmpty<SpeedBoostSignal>();
        }
    }
}