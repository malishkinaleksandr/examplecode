namespace GameScene.GameElements.CollisionSystem.CollisionRules
{
    public abstract class BaseRule
    {
        public abstract void SetRule();
    }
}