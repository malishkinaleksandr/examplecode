using Core.Signals;
using GameScene.Inventory.Configs;

namespace GameScene.GameElements.CollisionSystem.CollisionRules.InventoryRules
{
    public class GunRule : WeaponPickupRule
    {
        protected override EWeaponType WeaponType => EWeaponType.Gun;

        public GunRule(ISignalSender signalSender) : base(signalSender)
        {
        }
    }
}