using Core.Signals;
using GameScene.Inventory.Configs;

namespace GameScene.GameElements.CollisionSystem.CollisionRules.InventoryRules
{
    public class Pistol02Rule : WeaponPickupRule
    {    
        protected override EWeaponType WeaponType => EWeaponType.Pistol02;

        public Pistol02Rule(ISignalSender signalSender) : base(signalSender)
        {
        }
    }
}