using Core.Signals;
using GameScene.Inventory.Configs;

namespace GameScene.GameElements.CollisionSystem.CollisionRules.InventoryRules
{
    public class Pistol04Rule : WeaponPickupRule
    {
        protected override EWeaponType WeaponType => EWeaponType.Pistol04;

        public Pistol04Rule(ISignalSender signalSender) : base(signalSender)
        {
        }
    }
}