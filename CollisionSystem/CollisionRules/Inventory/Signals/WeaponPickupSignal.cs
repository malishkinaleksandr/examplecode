using Core.Signals.Domain;
using GameScene.Inventory.Configs;

namespace GameScene.GameElements.CollisionSystem.CollisionRules.Inventory.Signals
{
    public class WeaponPickupSignal : Signal
    {
        public EWeaponType WeaponType { get; set; }

        public override void Clear()
        {
            WeaponType = EWeaponType.None;
        }
    }
}