using GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules;

namespace GameScene.GameElements.CollisionSystem.CollisionTriggers
{
    public class BoostCollisionTrigger : BaseCollisionTrigger<BaseBoostRule>
    {
    }
}