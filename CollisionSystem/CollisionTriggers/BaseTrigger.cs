using UnityEngine;

namespace GameScene.GameElements.CollisionSystem.CollisionTriggers
{
    public abstract class BaseTrigger : BaseGameElement
    {
        public abstract void OnCollisionGameObject(Collider collider,
            GameObject otherGameObject);
    }
}