using System.Collections.Generic;
using Core.Signals;
using GameScene.GameElements.CollisionSystem.CollisionRules;
using GameScene.GameElements.CollisionSystem.Configs;
using GameScene.Status;
using UnityEngine;
using Zenject;

namespace GameScene.GameElements.CollisionSystem.CollisionSystems
{
    public abstract class BaseCollisionSystem<T> : ICollisionSystem<T> where T : BaseRule
    {
        [Inject] protected IGameStatus GameStatus;
        [Inject] protected ISignalSender SignalSender;
        
        protected Dictionary<ECollisionType, T> PoolInfo;
        
        protected abstract void SetupRules();
        
        public void Initialize()
        {
            SetupRules();
        }

        public void Dispose()
        {
            PoolInfo.Clear();
        }

        public void ApproveCollision(ECollisionType collisionType, GameObject otherGameObject)
        {
            if (PoolInfo.TryGetValue(collisionType, out var rule))
            {
                rule.SetRule();
            }
            else
            {
                 Debug.Log($"Can't find collision rule {collisionType}");
            }
        }
    }
}