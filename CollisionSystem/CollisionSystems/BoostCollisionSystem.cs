using System.Collections.Generic;
using GameScene.GameElements.CollisionSystem.CollisionRules.BoostRules;
using GameScene.GameElements.CollisionSystem.Configs;

namespace GameScene.GameElements.CollisionSystem.CollisionSystems
{
    public class BoostCollisionSystem : BaseCollisionSystem<BaseBoostRule>
    {
        protected override void SetupRules()
        {
            PoolInfo = new Dictionary<ECollisionType, BaseBoostRule>()
            {
                {ECollisionType.SpeedBoost, new SpeedRule(SignalSender)},
                {ECollisionType.JumpBoost, new JumpRule(SignalSender)},
                {ECollisionType.HealthBoost,new HealthRule(SignalSender)}
            };
        }
    }
}